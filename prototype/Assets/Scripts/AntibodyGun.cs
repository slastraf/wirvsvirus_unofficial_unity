﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntibodyGun : MonoBehaviour
{
    public Camera fpsCam;
    public float range = 50f;
    public float damage = 10f;
    public GameObject projectile;

    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Fire1")){
            Shoot();
        }
    }
    void Shoot(){
        RaycastHit hit;
        if(Physics.Raycast(gameObject.transform.position, fpsCam.transform.forward, out hit, range)){
            Debug.Log(hit.transform.name);
        
            GunTarget target = hit.transform.GetComponent<GunTarget>();
            if(target != null){
                target.TakeDamage(damage);
            }
            spawnAntibody();
        
        }

    
    }
    void spawnAntibody(){
        GameObject a = Instantiate(projectile) as GameObject;
        a.transform.position = gameObject.transform.position+fpsCam.transform.forward;
        a.transform.rotation = fpsCam.transform.rotation;
        if(a.GetComponent<Rigidbody>() != null){
            a.GetComponent<Rigidbody>().velocity = 100*gameObject.transform.forward;
        }
        Destroy(a, 1f);
    }
}