﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class virusMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public float lookRadius = 100f;
    public float acceleration;
    private Transform target;
    private GameObject victim;
    public PrintMessages _message;
    private Rigidbody rb;
    void Start()
    {

        target = FindClosestTarget().transform;
        rb = gameObject.GetComponent<Rigidbody>();
        
        
    }
    
    // Update is called once per frame
    void Update(){
       
        if(victim == null){
            victim = FindClosestTarget();
            if(victim == null){
                _message.AllCellsDestroyed();
                Destroy(gameObject);
            }
            target = victim.transform;
        }
        float distance = Vector3.Distance(target.position, transform.position);
            if(distance <= lookRadius){
            transform.LookAt(target);
            //rb.AddForce(transform.forward);
            rb.velocity += acceleration*(target.position-transform.position).normalized*Time.deltaTime;
            }
        
    }
    public GameObject FindClosestTarget(){
        float distance = Mathf.Infinity;
        GameObject[] targets = GameObject.FindGameObjectsWithTag("bodyCell");
        GameObject closest = null;
        foreach(GameObject bodyCell in targets){
            Vector3 diff = bodyCell.transform.position-transform.position;
            float curDistance = diff.sqrMagnitude;
            if(curDistance<distance){
                closest = bodyCell;
                distance = curDistance;
            }

        }
        return closest;
    }
    
    }


