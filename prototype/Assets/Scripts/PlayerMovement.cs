﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float Speed = 5f;

    private Camera mainCam;
    public Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        mainCam = Camera.main;
        rb = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    private void Update() {
        transform.rotation = mainCam.transform.rotation;
        rb.velocity = new Vector3() + 
            Speed * transform.forward * Input.GetAxisRaw("Vertical")
            + Speed * transform.right * Input.GetAxisRaw("Horizontal");
    }
}
