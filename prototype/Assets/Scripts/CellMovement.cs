﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellMovement : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody rb;
    public float velocity = 40f;
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity += velocity*Vector3.right;
    }
}
