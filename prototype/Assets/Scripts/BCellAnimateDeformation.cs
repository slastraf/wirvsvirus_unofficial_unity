﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class BCellAnimateDeformation : MonoBehaviour
{
    public double angularVelocity = 5d;
    public SkinnedMeshRenderer skinMeshRenderer;
    // Start is called before the first frame update
    private float standardAngle;
    void Start()
    {
        standardAngle = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        standardAngle += Time.deltaTime;
        skinMeshRenderer.SetBlendShapeWeight(0,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle)));
        skinMeshRenderer.SetBlendShapeWeight(1,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle+2d)));
        skinMeshRenderer.SetBlendShapeWeight(2,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle+3d)));
        skinMeshRenderer.SetBlendShapeWeight(3,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle+1d)));
        skinMeshRenderer.SetBlendShapeWeight(4,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle+0.3d)));
        skinMeshRenderer.SetBlendShapeWeight(5,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle+1.5d)));
        skinMeshRenderer.SetBlendShapeWeight(6,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle+1d)));
        skinMeshRenderer.SetBlendShapeWeight(7,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle+2.6d)));
        skinMeshRenderer.SetBlendShapeWeight(8,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle+2.1d)));
    
        //skinMeshRenderer.SetBlendShapeWeight(0, 50f);
    }
    
}
