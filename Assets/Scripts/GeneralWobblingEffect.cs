﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GeneralWobblingEffect : MonoBehaviour
{
    //This Script must be applied to the Mesh of the hull itself.
    public double angularVelocity;
    private SkinnedMeshRenderer skinMeshRenderer;
    private float currentAngle;
    private Mesh mesh;
    private int max;
    // Start is called before the first frame update
    void Start()
    {
        skinMeshRenderer = (SkinnedMeshRenderer)GetComponentInChildren(typeof(SkinnedMeshRenderer));
        if(skinMeshRenderer==null){
            Debug.Log("skinMeshRenderer is missing");
        }
        mesh = GetComponent<MeshFilter>().mesh;
        max = mesh.blendShapeCount;
      
    }

    // Update is called once per frame
    void Update()   
    {
        currentAngle += Time.deltaTime;
        for(int i = 0; i < max; i++){
            skinMeshRenderer.SetBlendShapeWeight(i,100f*(float)Math.Abs(Math.Sin(angularVelocity*currentAngle+i*0.5f)));
        }
    }
}
