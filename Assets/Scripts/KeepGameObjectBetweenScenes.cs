﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepGameObjectBetweenScenes : MonoBehaviour
{
    
    void Awake(){
        DontDestroyOnLoad(gameObject);
    }
}
