﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunTarget : MonoBehaviour
{
    public ScoreScript _scoreScript;
    public float health = 20f;
    void Start(){
      
    }
    public void TakeDamage(float amount){
        health -= amount;
        if(health <= 0f){
            Die();
        }
    }
    void Die(){
        _scoreScript.increaseScore();
        Destroy(gameObject);
    }
    
}
