﻿
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    public float restartDelay = 1f;
    bool gameHasEnded;
    public void EndGame(){
        if(gameHasEnded == false){
            gameHasEnded = true;
            Debug.Log("GAME OVER");
            Invoke("Restart",restartDelay);
        }
    }
    public void Restart(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void WinGame(){
        Debug.Log("You won!");
        SceneManager.LoadScene(0);
    }
}
