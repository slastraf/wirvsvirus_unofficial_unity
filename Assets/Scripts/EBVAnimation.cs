﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class EBVAnimation : MonoBehaviour
{
    public double angularVelocity = 6d;
    public SkinnedMeshRenderer skinMeshRenderer;
    private float standardAngle = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        standardAngle += Time.deltaTime;
        skinMeshRenderer.SetBlendShapeWeight(0,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle)));
        skinMeshRenderer.SetBlendShapeWeight(1,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle+2d)));
        skinMeshRenderer.SetBlendShapeWeight(2,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle+3d)));
        skinMeshRenderer.SetBlendShapeWeight(3,50f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle+1d)));
        skinMeshRenderer.SetBlendShapeWeight(4,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle+0.3d)));
        skinMeshRenderer.SetBlendShapeWeight(5,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle+1.5d)));
        skinMeshRenderer.SetBlendShapeWeight(6,100f*(float)Math.Abs(Math.Sin(angularVelocity*standardAngle+1d)));
    }
}
