﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Virus : MonoBehaviour
{

    public float rotSpeed = 0.1f;
    private Quaternion targetRotation;

    // Start is called before the first frame update
    void Start()
    {
        //Zufällige startrotation generieren
        var euler = transform.eulerAngles;
        euler.z = Random.Range(0.0f, 360.0f);
        transform.eulerAngles = euler;

        InvokeRepeating("randomRot", 0f, 1f);
        InvokeRepeating("updateRot", 0f, 0.05f);
    }

    // Update is called once per frame
    void randomRot()
    {
        targetRotation = new Quaternion(Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f), 0);
    }

    void updateRot()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotSpeed * Time.deltaTime);
        transform.position += new Vector3(0,0,Mathf.Sin(Time.deltaTime));
    }
}
