﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//this script should be applicable to every virus GameObject
public class GeneralVirusMovement : MonoBehaviour
{   public Transform target;
    public float acceleration;
    public float lookRadius = 50f;
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);
            if(distance <= lookRadius){
            transform.LookAt(target);
            //rb.AddForce(transform.forward);
            rb.velocity += acceleration*(target.position-transform.position).normalized*Time.deltaTime;
            }
    }
}
