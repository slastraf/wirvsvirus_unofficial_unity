﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Player : MonoBehaviour
{
    // Start is called before the first frame update
    public int level = 0;
    public int hitpoints = 100;
    public Transform playerTransform;
    public string equippedAntibodies;
    private TextWrapper showAntibody;
    void Start(){
        GameObject firstCheck = GameObject.FindWithTag("AntibodyDisplay");
        
        showAntibody = firstCheck.GetComponent<TextWrapper>();
        
    }
    void Update(){
        playerTransform = gameObject.transform;
        Debug.Log(equippedAntibodies);
    }
    public void changeAntibody(string name){
        equippedAntibodies = name;
        showAntibody.changeText("Antibody: "+name);
    }

    public void SavePlayer(){
        Debug.Log("Save Player Data");
        SaveSystem.SavePlayer(this);
    }
    public void LoadPlayer(){
        PlayerData data = SaveSystem.LoadPlayer();
        level = data.level;
        hitpoints = data.health;
        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];
        transform.position = position;
    }
}
