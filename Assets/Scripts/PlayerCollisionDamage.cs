﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerCollisionDamage : MonoBehaviour
{
    public Text deathSign;// YOU DIED!
    private int hitpoints;
    public Text HitpointCount;
    private Player thePlayer;
    // Start is called before the first frame update
    void Start()
    {
        
        thePlayer = GetComponent<Player>();
        deathSign.gameObject.SetActive(false);
        hitpoints = thePlayer.hitpoints;
        
        HitpointCount.text = "HP: "+hitpoints;
    }
    void Update(){
        HitpointCount.text = "HP: "+hitpoints;
        
    }
    // Update is called once per frame
    void OnTriggerEnter(Collider other){
        if(other.gameObject.tag=="EBV"){

            //If this happens, the player is f*****
            hitpoints -= 3;
            HitpointCount.text = "HP: "+hitpoints;
        }
        else if(other.gameObject.tag=="THelperCell"){
            Debug.Log("collision with T Cell detected");
            thePlayer.changeAntibody(other.gameObject.GetComponent<THelperCell>().antibodies);
            if(other.gameObject.GetComponent<THelperCell>().antibodies == null){
                Debug.Log("Mist");
            }
            Debug.Log("reached ENd");
        }
        if(hitpoints <= 0){
            PlayerDied();
        }
    }
    void PlayerDied(){
            deathSign.gameObject.SetActive(true);
            FindObjectOfType<GameManager>().EndGame();

    }
}
