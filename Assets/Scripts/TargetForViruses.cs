﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
public class TargetForViruses : MonoBehaviour
{
    public float hitpoints;
    private float hits = 0f;
    private GameObject coronaVirus;//just put an instance of the virus in here.
    // Start is called before the first frame update
    public bool infected = false;
    void Start()
    {
        coronaVirus = (GameObject)Resources.Load<GameObject>("virus");
        if(coronaVirus==null){
          
            Debug.Log("corona Virus ist null");
        }
    }

   
    void OnTriggerEnter(Collider other){
        if(other.gameObject.tag == "virus"){
            hitpoints -=1f;
          
            if(0 >= hitpoints){
                CoronaInfection();
                }
        }
    }
    private void CoronaInfection(){
        infected = true;
        int i = 0;
        while(i < 20){
            Invoke("SpawnVirion",0.5f);
            i++;
            //Thread.Sleep(2000);
        }
            
        Destroy(gameObject);
        
    }
    private void SpawnVirion(){
         GameObject a = Instantiate(coronaVirus) as GameObject;
        a.transform.position = transform.position;
    }
}
