﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PrintMessages : MonoBehaviour
{
    Text message;
    bool cellsAlive = true;
    // Start is called before the first frame update
    void Start()
    {
        message = GetComponent<Text>();
    
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void AllCellsDestroyed(){
        if(cellsAlive){
            message.text = "All Cells have been destroyed";
            cellsAlive = false;
            }
    }
    public void AllVirusesDestroyes(){
        message.text = "Every virus has been destroyed";
    }
    public void ConnectionSecured(){
        //for testing purposes only.
        message.text = "The dark side of the force is a pathway to many abilities,...";
    }

}
