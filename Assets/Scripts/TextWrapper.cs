﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TextWrapper : MonoBehaviour
{
    public Text myText;
    public void Start(){
        myText = (Text) GetComponent<Text>();
    }
    public void changeText(string input){
        myText.text = input;
    }
}
