﻿using System;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private const float Y_ANGLE_MIN = -89.0f;
    private const float Y_ANGLE_MAX = 89.0f;

    public Transform lookAt;//Transform saves position, rotation and scale

    private Vector3 desiredCameraPosition;

    public float distance = 10;
    private float currentX = 0f;
    private float currentY = 0f;
    public float sensivityX = 4.0f;
    public float sensivityY = 4.0f;

    public bool inverted = false;

    private void Update() {
        currentX += Input.GetAxis("Mouse X") * sensivityX;
        currentY += Input.GetAxis("Mouse Y") * sensivityY;

        currentY = Mathf.Clamp(currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);
    }

    private void LateUpdate() {//LateUpdate is called after Update, once per Frame
        Vector3 dir = new Vector3(0, 0, -distance);
        Quaternion rotation = inverted ? Quaternion.Euler(-currentY, currentX, 0) : Quaternion.Euler(currentY, currentX, 0);

        //check if camera collides
        RaycastHit hit;
        desiredCameraPosition = lookAt.position + rotation * dir;
        if (Physics.Linecast(lookAt.position, desiredCameraPosition, out hit, ~(1 << LayerMask.NameToLayer("Player")))) {
            //Debug.Log(hit.distance.ToString());
            transform.position = lookAt.position + rotation * new Vector3(0,0, -hit.distance * 0.9f);
        } else {
            transform.position = desiredCameraPosition;
        }

        // rotate towards player
        transform.LookAt(lookAt.position);
    }
}
